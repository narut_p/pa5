import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * A GUI class that show the time and the button.
 * @author Narut Poovorakit
 * @version 11.04.2015
 */
public class UI extends JFrame {
	// Create panel, label, button, timer and clock.
	private JPanel frame, panelUp, panelDown;
	private JLabel pos1, pos2, pos3, pos4, pos5, pos6, semicolon1, semicolon2;
	private JButton setBut, addBut, minusBut;
	private Clock clock;
	javax.swing.Timer timer;

	/**
	 * A constructor method
	 * @param clock
	 */
	public UI(Clock clock) {
		super("Digital Clock");
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		super.setBounds(0, 0, 200, 140);
		this.clock = clock;
		super.setVisible(true);
		
		initialize();
	}

	/**
	 * Initialize the sound that alarm.
	 */
	public void alarm() {

		try {
			Clip clip = AudioSystem.getClip();
			AudioInputStream input = AudioSystem.getAudioInputStream(Main.class
					.getResource("sound/Sound.wav"));
			clip.open(input);
			clip.start();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Initialize all of the panel.
	 */
	public void initialize() {
		// Panel
		frame = new JPanel();
		panelUp = new JPanel();
		panelDown = new JPanel();

		// Button
		setBut = new JButton("Set");
		addBut = new JButton("+");
		minusBut = new JButton("-");

		// Set layout
		frame.setLayout(new BoxLayout(frame, BoxLayout.Y_AXIS));
		panelUp.setLayout(new FlowLayout());
		panelDown.setLayout(new FlowLayout());

		// Label
		pos1 = new JLabel();
		pos2 = new JLabel();
		pos3 = new JLabel();
		pos4 = new JLabel();
		pos5 = new JLabel();
		pos6 = new JLabel();
		semicolon1 = new JLabel(" : ");
		semicolon2 = new JLabel(" : ");

		ImageIcon[] imgIcon = getImageIcon();

		int delay = 1000; // milliseconds

		ActionListener task = new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				clock.getState().setTime();
				if (clock.getState() == clock.Displaytime) {
					pos1.setIcon(imgIcon[clock.getHour() / 10]);
					pos2.setIcon(imgIcon[clock.getHour() % 10]);
					pos3.setIcon(imgIcon[clock.getMinute() / 10]);
					pos4.setIcon(imgIcon[clock.getMinute() % 10]);
					pos5.setIcon(imgIcon[clock.getSecond() / 10]);
					pos6.setIcon(imgIcon[clock.getSecond() % 10]);

				} else {
					pos1.setIcon(imgIcon[clock.getAlarmHour() / 10]);
					pos2.setIcon(imgIcon[clock.getAlarmHour() % 10]);
					pos3.setIcon(imgIcon[clock.getAlarmMinute() / 10]);
					pos4.setIcon(imgIcon[clock.getAlarmMinute() % 10]);
					pos5.setIcon(imgIcon[clock.getAlarmSecond() / 10]);
					pos6.setIcon(imgIcon[clock.getAlarmSecond() % 10]);

				}

				if (clock.getAlarmHour() == clock.getHour()
						&& clock.getAlarmMinute() == clock.getMinute()
						&& clock.getAlarmSecond() == clock.getSecond()) {
					alarm();
				}

			}
		};
		timer = new javax.swing.Timer(delay, task);

		semicolon1.setForeground(Color.WHITE);
		semicolon2.setForeground(Color.WHITE);
		panelUp.add(pos1);
		panelUp.add(pos2);
		panelUp.add(semicolon1);
		panelUp.add(pos3);
		panelUp.add(pos4);
		panelUp.add(semicolon2);
		panelUp.add(pos5);
		panelUp.add(pos6);

		panelDown.add(setBut);
		setBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clock.getState().performSet();

			}
		});

		//Add button
		panelDown.add(addBut);
		addBut.setBackground(Color.DARK_GRAY);
		addBut.setForeground(Color.WHITE);
		addBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (clock.getState() == clock.SetHour) {
					if (clock.getAlarmHour() == 23) {
						clock.setAlarmHour(-1);
					}
					clock.SetHour.add();
					pos1.setIcon(imgIcon[clock.getAlarmHour() / 10]);
					pos2.setIcon(imgIcon[clock.getAlarmHour() % 10]);
				}

				if (clock.getState() == clock.SetMinute) {
					clock.SetMinute.add();
					pos3.setIcon(imgIcon[clock.getAlarmMinute() / 10]);
					pos4.setIcon(imgIcon[clock.getAlarmMinute() % 10]);
					if (clock.getAlarmMinute() == 59) {
						clock.setAlarmMinute(-1);
					}
				}

				if (clock.getState() == clock.SetSecond) {
					clock.SetSecond.add();
					pos5.setIcon(imgIcon[clock.getAlarmSecond() / 10]);
					pos6.setIcon(imgIcon[clock.getAlarmSecond() % 10]);
					if (clock.getAlarmSecond() == 59) {
						clock.setAlarmSecond(-1);
					}
				}

			}
		});

		// Subtract button.
		panelDown.add(minusBut);
		minusBut.setBackground(Color.DARK_GRAY);
		minusBut.setForeground(Color.WHITE);
		minusBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (clock.getState() == clock.SetHour) {
					if (clock.getAlarmHour() == 0) {
						clock.setAlarmHour(24);
					}
					clock.SetHour.subtract();
					pos1.setIcon(imgIcon[clock.getAlarmHour() / 10]);
					pos2.setIcon(imgIcon[clock.getAlarmHour() % 10]);
				}

				if (clock.getState() == clock.SetMinute) {
					if (clock.getAlarmMinute() == 0) {
						clock.setAlarmMinute(60);
					}
					clock.SetMinute.subtract();
					pos3.setIcon(imgIcon[clock.getAlarmMinute() / 10]);
					pos4.setIcon(imgIcon[clock.getAlarmMinute() % 10]);

				}

				if (clock.getState() == clock.SetSecond) {
					if (clock.getAlarmSecond() == 0) {
						clock.setAlarmSecond(60);
					}
					clock.SetSecond.subtract();
					pos5.setIcon(imgIcon[clock.getAlarmSecond() / 10]);
					pos6.setIcon(imgIcon[clock.getAlarmSecond() % 10]);

				}

			}
		});
		panelUp.setBackground(Color.BLACK);
		panelDown.setBackground(Color.BLACK);
		frame.add(panelUp);
		frame.add(panelDown);
		super.add(frame);

	}

	public ImageIcon[] getImageIcon() {
		Class cls = this.getClass();
		ImageIcon[] imgIcon = new ImageIcon[10];
		for (int i = 0; i < 10; i++) {
			URL digit = cls.getResource("images/" + i + ".png");
			ImageIcon icon = new ImageIcon(digit);
			imgIcon[i] = icon;
		}
		return imgIcon;
	}

	public void run() {
		timer.start();

	}

}
