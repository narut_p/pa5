/**
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015 A class that set the hour.
 */
public class SetHour implements State {
	// Create a clock.
	private Clock clock;

	/**
	 * Set a new clock.
	 * 
	 * @param clock
	 */
	public SetHour(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Empty method.
	 */
	@Override
	public void setTime() {
		// TODO Auto-generated method stub

	}

	/**
	 * Set when change the state.
	 */
	@Override
	public void performSet() {
		clock.setState(clock.SetMinute);
	}

	/**
	 * add the hour by 1.
	 */
	@Override
	public void add() {
		clock.setAlarmHour(clock.getAlarmHour() + 1);

	}

	/**
	 * subtract hour by 1.
	 */
	@Override
	public void subtract() {
		clock.setAlarmHour(clock.getAlarmHour() - 1);
	}

}
