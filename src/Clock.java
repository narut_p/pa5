import java.util.Calendar;

/**
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015 A class clock that a main of all class contain hour,
 *          minute, second, alarm hour, alarm minute, alarm second, and all of
 *          the state such as setHour, setMinute, setSecond, DisplayTime and
 *          DisplayAlarmTime.
 *
 */
public class Clock {
	private int hour, minute, second;
	private int alarmHour, alarmMinute, alarmSecond;
	private State state;
	State SetMinute = new SetMinute(this);
	State SetSecond = new SetSecond(this);
	State SetHour = new SetHour(this);
	State DisplayAlarmTIme = new DisplayAlarmTime(this);
	State Displaytime = new DisplayTime(this);

	/**
	 * A constructor method that set the value.
	 */
	public Clock() {
		state = Displaytime;
		hour = 0;
		minute = 0;
		second = 0;
		alarmHour = 0;
		alarmMinute = 0;
		alarmSecond = 0;
	}

	/**
	 * update time of the present in Thailand.
	 */
	public void updateTime() {
		Calendar calendar = Calendar.getInstance();
		hour = calendar.get(calendar.HOUR_OF_DAY);
		minute = calendar.get(calendar.MINUTE);
		second = calendar.get(calendar.SECOND);

	}

	/**
	 * Set a state.
	 * 
	 * @param state
	 *            .
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * Get a state.
	 * 
	 * @return state.
	 */
	public State getState() {
		return this.state;
	}

	/**
	 * Get an hour.
	 * 
	 * @return an hour.
	 */
	public int getHour() {
		return this.hour;
	}

	/**
	 * Get a minute.
	 * 
	 * @return minute.
	 */
	public int getMinute() {
		return this.minute;
	}

	/**
	 * Get a second.
	 * 
	 * @return second.
	 */
	public int getSecond() {
		return this.second;
	}

	/**
	 * Get an alarm hour.
	 * 
	 * @return an alarm hour.
	 */
	public int getAlarmHour() {
		return this.alarmHour;
	}

	/**
	 * Get an alarm minute.
	 * 
	 * @return an alarm minute.
	 */
	public int getAlarmMinute() {
		return this.alarmMinute;
	}

	/**
	 * Get an alarm second.
	 * 
	 * @return an alarm second.
	 */
	public int getAlarmSecond() {
		return this.alarmSecond;
	}

	/**
	 * Set an hour.
	 * 
	 * @param hour
	 */
	public void setHour(int hour) {
		this.hour = hour;
	}

	/**
	 * Set a minute.
	 * 
	 * @param minute
	 */
	public void setMinute(int minute) {
		this.minute = minute;
	}

	/**
	 * Set a second.
	 * 
	 * @param second
	 */
	public void setSecond(int second) {
		this.second = second;
	}

	/**
	 * Set an alarm hour.
	 * 
	 * @param alarmHour
	 */
	public void setAlarmHour(int alarmHour) {
		this.alarmHour = alarmHour;
	}

	/**
	 * Set an alarm minute.
	 * 
	 * @param alarmMinute
	 */
	public void setAlarmMinute(int alarmMinute) {
		this.alarmMinute = alarmMinute;
	}

	/**
	 * Set an alarm second.
	 * 
	 * @param alarmSecond
	 */
	public void setAlarmSecond(int alarmSecond) {
		this.alarmSecond = alarmSecond;
	}
}
