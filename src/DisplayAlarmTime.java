/**
 * A class that display an alarm time.
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015
 *
 */
public class DisplayAlarmTime implements State {
	private Clock clock;

	/**
	 * A constuctor method.
	 * 
	 * @param clock
	 */
	public DisplayAlarmTime(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Empty method
	 */
	@Override
	public void setTime() {

	}

	/**
	 * Empty method
	 */
	@Override
	public void performSet() {

	}

	/**
	 * Add and change a new state.
	 */
	@Override
	public void add() {
		clock.setState(clock.Displaytime);
	}

	/**
	 * Empty method.
	 */
	@Override
	public void subtract() {
		// TODO Auto-generated method stub

	}

}
