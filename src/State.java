/**
 * An interface class
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015
 *
 */
public interface State {

	public void setTime();

	public void performSet();

	public void add();

	public void subtract();
}
