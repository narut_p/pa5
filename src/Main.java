/**
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015
 * A main method that run the clock.
 */
public class Main {
	public static void main(String[] args) {
		// Create a clock, ui, and run the ui.
		Clock clock = new Clock();
		UI ui = new UI(clock);
		ui.run();
	}
}
