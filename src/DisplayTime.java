/**
 * A display time class that show the present time in hour, minute and second.
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015
 *
 */
public class DisplayTime implements State {
	private Clock clock;

	/**
	 * A constructor method
	 * 
	 * @param clock
	 */
	public DisplayTime(Clock clock) {
		this.clock = clock;

	}

	/**
	 * Set a new time that already update.
	 */
	@Override
	public void setTime() {
		clock.updateTime();
	}

	/**
	 * Set a new state.
	 */
	@Override
	public void performSet() {
		clock.setState(clock.SetHour);

	}

	/**
	 * When click add its turn new state.
	 */
	@Override
	public void add() {
		clock.setState(clock.DisplayAlarmTIme);

	}

	/**
	 * Empty method.
	 */
	@Override
	public void subtract() {
		// TODO Auto-generated method stub

	}

}
