/**
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015 A state class that set a minute.
 *
 */
public class SetMinute implements State {
	// Create clock
	private Clock clock;

	/**
	 * set new clock.
	 * 
	 * @param clock
	 */
	public SetMinute(Clock clock) {
		this.clock = clock;
	}

	/**
	 * empty method.
	 */
	@Override
	public void setTime() {
		// TODO Auto-generated method stub

	}

	/**
	 * Change a state when set.
	 */
	@Override
	public void performSet() {
		clock.setState(clock.SetSecond);

	}

	/**
	 * Add the minute by 1.
	 */
	@Override
	public void add() {
		clock.setAlarmMinute(clock.getAlarmMinute() + 1);
	}

	/**
	 * Subtract the minute by 1.
	 */
	@Override
	public void subtract() {
		clock.setAlarmMinute(clock.getAlarmMinute() - 1);
	}

}
