/**
 * 
 * @author Narut Poovorakit
 * @version 11.04.2015
 * A state class that set a second.
 *
 */
public class SetSecond implements State {
	// Create a clock.
	private Clock clock;

	/**
	 * Set a second.
	 * @param clock
	 */
	public SetSecond(Clock clock) {
		this.clock = clock;
	}

	/**
	 * Empty method.
	 */
	@Override
	public void setTime() {
		// TODO Auto-generated method stub

	}

	/**
	 * Change state by set.
	 */
	@Override
	public void performSet() {
		clock.setState(clock.Displaytime);

	}

	/**
	 * Add second by 1.
	 */
	@Override
	public void add() {
		clock.setAlarmSecond(clock.getAlarmSecond() + 1);
	}

	/**
	 * Subtract second by 1.
	 */
	@Override
	public void subtract() {
		clock.setAlarmSecond(clock.getAlarmSecond() - 1);
	}

}
